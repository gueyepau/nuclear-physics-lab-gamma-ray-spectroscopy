# Nuclear Physics Lab - Gamma Ray Spectroscopy

The Gamma Ray Spectroscopy virtual laboratory is part of the 2021 Physics Immersion Program to provide basic understanding in experimental nuclear physics. It is heavily designed after the MSU SS21-PHY-451 course. 

**Gamma Ray Spectroscopy**

Gamma rays (or $`\gamma`$-ray) are photons that originate from nuclear de-excitation and have typically energies of few MeV. They carry information on nuclear binding and energy levels. Gamma rays have a plethora of applications ranging from nuclear asrtophysics (such as "gamma-ray bursters" that seem to be telling us something about the mechanics of stellar collapse) to health (such as cancer tretament and imaging) amongst others. In this experiment, students will establish the function of a NaI crystal + photomultiplier system to detect gamma-rays, and use the system to study various radio-isotopes. Of particular interest are: Co-60 that is used for medical radiation treatment and is a standard gamma ray reference source and Na-22 that undergoes $`\beta`$ decay to positrons and where the gammas rays result from positron annihilattion with atomic electrons. The  gamma ray energy spectra also contain information about how gamma rays interact with matter.

**What you will learn**

+ Detection and measurement of gamma rays
+ NaI crystal photomultiplier, NIM electronics, computer controlled ADC and histogramming with the Amptek "Pocket MCA"
+ Interactions of photons with matter: Compton effect, photoelectric effect
+ Mass of the electron via photon energy when electron annihilates with positron

**Preparation**

+ Poisson Statistics
  + [Instructions from Junior Lab @ MIT](http://web.mit.edu/8.13/www/intro3.shtml)
+ Some other reading:
  + [Procedure for Gamma Ray Spectroscopy](Documents/guide_Gamma_ray_updated.pdf)
  + [Detecting Nuclear Radiation](Documents/guide_gamma-gamma_coincidence.pdf)
+ Recommended reading: 
  + A. Melissinos and J. Napolitano, Experiments in Modern Physics, 2nd Ed. 8.1-8.2.5 and 8.4. 
  + G. Gilmore and J. Hemingway, Practical Gamma-ray Spectrometry (A great book crammed with useful information.)
  + W.R. Leo, Techniques for Nuclear and Particle Physics Experiments
  + G.F. Knoll, Radiation Detection and Measurement, Chapter
+ Supplementary materials
  + [Some notes on nuclear detection techniques](Documents/notes_detecting_nuclear_radiation.pdf)
  + References:
    + [1936 Nobel Prize for V. F. Hess's discovery of cosmic radiation and C. D. Andern's discover of the positron](https://www.nobelprize.org/prizes/physics/1936/summary/)
    + [Online database of radioactive nuclei](https://www.nndc.bnl.gov/nudat2/) ( this is a very rich site)
    + [SWIFT](http://swift.gsfc.nasa.gov/docs/swift/swiftsc.html) and [GLAST](http://www-glast.stanford.edu/): new satellites  to observe gamma ray bursts
    + [ROTSE](http://www.rotse.net/) is worldwide network to see optical follow-up to gamma-ray burst. Invented and based at Michigan.
    + http://hires.physics.utah.edu/ (The Fly's Eye Cosmic Ray Experiment)
  + Instruments:
    + [Manual - Canberra 802-5](Documents/instruments_Canberra_802-5.pdf)
    + [Manual - Canberra PMT base](Documents/instruments_Canberra_2007-2007P-SS-CSP0071_PMT_base.pdf)
    + [Manual - HPGE 21-P801A](Documents/instruments_HPGE_21-P801A_qad.pdf)
    + [Manual - Tektronix TDS 3000 series oscilloscope](Documents/manual_Tektronix_TDS3000B.pdf)
