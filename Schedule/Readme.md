# Schedule

+ June 28, 2021
  + Brief Overview
+ June 29, 2021 (moved to June 30, 2021)
  + Goals
    + Perform channel to energy conversion on Co-60 and Cs-137 data set from a Sodium Iodide (NaI) and a High Purity Germanium (HPGe) detectors
    + Write a python script to read, graph and fit the data
    + Use of the [NNDC database](https://www.nndc.bnl.gov) to identify relevant $`\gamma`$-ray peaks 
  + Procedure
    + Co-60 analysis
      + Plot the Co-60 data set: counts vs. channel
      + Identify the counts and channels for the two $`\gamma`$-rays
      + Verify that the energy of the $`\gamma`$-rays from the NNDC database
      + Calculate the slope of the line between the two points: this is your channel-to-energy calibration factor
      + Perform a linear fit using python and verify that you have the same value
      + Plot the calibrated Co-60 data set by applying the calibration factor to the channel axis
    + Cs-137 analysis
      + Calibrate the Cs-137 data using the same calibration factor
      + Verify that the $`\gamma`$-ray is at the correct location from the NNDC database
  + NaI data set: they are accessible from the [NaI Data folder](https://gitlab.msu.edu/gueyepau/nuclear-physics-lab-gamma-ray-spectroscopy/-/tree/master/ExperimentalDataVideos/NaI%20Data)
  + HPGe data set: they are accessible from the [HPGe Data folder](https://gitlab.msu.edu/gueyepau/nuclear-physics-lab-gamma-ray-spectroscopy/-/tree/master/ExperimentalDataVideos/HPGe%20Data)
  + Zoom recording: accessible at [Physics Lab Zoom Recordings](https://drive.google.com/drive/folders/1I4HRFHu07oYeVPZTFfUEMXt0K0Z7kh7d?usp=sharing)
+ June 30, 2021 (moved to July 1, 2021)
  + Goals
    + Identification of $`\gamma`$-ray emissions for several radio-nuclei measured with the NaI detector
    + Calculate the resolution for each $`\gamma`$-ray peak
  + Procedure
    + Calibrate each radio-isotope data measured with the NaI detector using the same procedure as above
    + Verify that the $`\gamma`$-rays are at the correct location from the NNDC database
    + Use python to fit each peak with a Gaussian distribution
      + Extract the mean ($`E_0`$) and standard deviation ($`\sigma`$) values
      + Calculate the resolution for each peak
      + Plot the resolution as a function of the $`\gamma`$-ray energy peak
  + NaI data set: they are accesible from the [NaI Data folder](https://gitlab.msu.edu/gueyepau/nuclear-physics-lab-gamma-ray-spectroscopy/-/tree/master/ExperimentalDataVideos/NaI%20Data)
  + Zoom recording: accessible at [Physics Lab Zoom Recordings](https://drive.google.com/drive/folders/1I4HRFHu07oYeVPZTFfUEMXt0K0Z7kh7d?usp=sharing)
+ July 01, 2021
  + Geant4 simulation (if time permits)
  + Review and Discussion


